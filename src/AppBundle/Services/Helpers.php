<?php

namespace AppBundle\Services;

class Helpers{

    public $manager;

    public function __construct($manager){
        $this->manager = $manager;
    }

    /* convertir la información a JSON
     *
     */

    public function json($data){
        $normalized = array(new \Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer());
        $encoders = array("json" => new \Symfony\Component\Serializer\Encoder\JsonEncode());

        $serialized = new \Symfony\Component\Serializer\Serializer($normalized, $encoders);
        $json = $serialized->serialize($data,'json');

        $response = new \Symfony\Component\HttpFoundation\Response();
        $response->setContent($json);
        $response->headers->set('Content-type', 'application/json');

        return $response;
    }
}

