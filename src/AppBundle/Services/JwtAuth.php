<?php
namespace AppBundle\Services;
use Firebase\JWT\JWT;

class JwtAuth{

    public $manager;
    public $key;

    public function __construct($manager){
        $this->manager = $manager;
        $this->key = 'holaqueases123456789?';

    }

    /**
     * Metodo que devuelve el login del usuario.
     * @param $email
     * @param $password
     * @return string
     */
    public function signup($email,$password,$getHash = null){//que getHash sea opcional

        //cargar el repositorio para usar el entityManager
        //devolverá cuando el email y el password coincidan con los de la BD

        $user = $this->manager->getRepository('BackendBundle:User')->findOneBy(array(
            "email" => $email,
            "password" => $password
        ));

        $signup = false;
        if(is_object($user)){
            $signup = true;
        }



        if($signup == true){
            //GENERAR TOKEN JWT con validez de tiempo

            $token = array(
                'sub' => $user->getId(), //sib se utiliza normalmente para el id
                "email" => $user->getEmail(),
                "name" => $user->getName(),
                "surname" => $user->getSurname(),
                "iat" => time(), //tiempo que empueza el token
                "exp" => time()+ (7*24*60*60) //fecha que expira (1 a la semana)
            );

            //codificamos la información
            $jwt = JWT::encode($token,$this->key, 'HS256');//codifica los valores con la key
            $decode = JWT::decode($jwt,$this->key, array('HS256'));

            /* si esta a NUll devolver el token */
            if($getHash == null){
                $data = $jwt;
            }else{//decodificar
                $data = $decode;
            }

            //$data = $jwt;
            /*
            $data = array(
                'status' => 'Success',
                'user' => $user
            );
            */
        }else{
            $data = array(
                'status' => 'Error',
                'data' => 'login failed'
            );

        }
        return $data;
    }


    //metodo que comprueba ese token es valido
    public function checkToken($jwt, $getIdentity = false){
        $auth = false;
        try{
            //decodificar el token(Token, nuestro token y el cifrado)
        $decode = JWT::decode($jwt, $this->key, array('HS256'));
        }catch(\UnexpectedValueException $e){
            $auth = false;
        }catch(\DomainException $e){
            $auth = false;

        }
        //si es un object y sub para sacar el ID
        if(isset($decode) && is_object($decode) && isset($decode->sub)){
            $auth = true;
        }else{
            $auth = false;
        }
        //comprobar si el getidentity contiene algo
        if($getIdentity == false){
            return $auth;
        }else{
            return $decode;

        }

    }
}
